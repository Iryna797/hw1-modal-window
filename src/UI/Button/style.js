import styled from 'styled-components';

export const MyButton = styled.button`
  cursor: pointer;
  border: none;
  padding: 10px 15px;
  color: #fff;
  text-transform: uppercase;
  border-radius: 5px;
  transition: all 0.3s;
  background-color: #5661e1;

  &:hover {
    filter: contrast(150%);
  }
`;
