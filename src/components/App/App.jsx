import React, { Component } from 'react';
import { Button } from '../../UI/Button/Button';
import { Modal } from '../../UI/Modal/Modal';
import { AppContainer, GlobalStyle } from './style.js';

export class App extends Component {
  constructor() {
    super();

    this.state = {
      modalIsVisible1: false,
      modalIsVisible2: false,
    };
  }

  render() {
    return (
      <AppContainer>
        <GlobalStyle />
        <Button
          className={'green'}
          text="Open first modal"
          backgroundColor="#b3382c"
          onClick={() => this.setState({ modalIsVisible1: true })}
        />
        {this.state.modalIsVisible1 && (
          <Modal
            className={'red'}
            setVisible={() => this.setState({ modalIsVisible1: false })}
            header={<>Do you want to delete this file ?</>}
            closeButton={true}
            text={
              <>
                <p>
                  Once you delete this file, it won't be posible to undo thiss
                  action.
                </p>
                <p>Are you sure you want to delete it ?</p>
              </>
            }
            action={
              <>
                <Button
                  className={'red'}
                  text="OK"
                  backgroundColor="#b3382c"
                  onClick={() => console.log('OK')}
                />
                <Button
                  className={'red'}
                  text="Cancel"
                  backgroundColor="#b3382c"
                  onClick={() => this.setState({ modalIsVisible1: false })}
                />
              </>
            }
          />
        )}

        <Button
          className={'green'}
          text="Open second modal"
          backgroundColor="#1daa1d"
          onClick={() => this.setState({ modalIsVisible2: true })}
        />
        {this.state.modalIsVisible2 && (
          <Modal
            className={'green'}
            setVisible={() => this.setState({ modalIsVisible2: false })}
            header={<>Are you 18 years old?</>}
            closeButton={false}
            text={
              <>
                <p>
                This site may contain adult content
                </p>
                <p>If not, press NO</p>
              </>
            }
            action={
              <>
                <Button
                  className={'blue'}
                  text="YES"
                  backgroundColor="#277af6"
                  width="55px"
                  onClick={() => this.setState({ modalIsVisible2: false })}
                />
                <Button
                  className={'red'}
                  text="NO"
                  backgroundColor="#f21c0d"
                  width="55px"
                  onClick={() => this.setState({ modalIsVisible2: false })}
                />
              </>
            }
          />
        )}
      </AppContainer>
    );
  }
}
